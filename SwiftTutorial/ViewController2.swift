//
//  ViewController2.swift
//  SwiftTutorial
//
//  Created by Admin on 03/08/16.
//  Copyright © 2016 aktuna. All rights reserved.
//

import Foundation
import UIKit


class ViewController2: UIViewController {
    
    let baseApiUrl = "http://52.28.140.212"
    
    
    var loadingView = UIView()
    
    //MARK:LifeCycle
    override func viewDidLoad() {
        self.initiateLoadingView(self.view)
        self.getDataFromService()
    }
    
    //MARK:Class Methods
    func getDataFromService()  {
        
        /*let defaultSession = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration())

        var dataTask: NSURLSessionDataTask?
        if dataTask != nil {
            dataTask?.cancel()
        }
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true

        let urlString = String(format: "%@%@", baseApiUrl,"/api/v1/product/list")

        let dataUrl = NSURL(string: urlString)
        
        dataTask = defaultSession.dataTaskWithURL(dataUrl!) {
            data, response, error in
          
            dispatch_async(dispatch_get_main_queue()) {
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            }
            
            if let error = error {
                print(error.localizedDescription)
            } else if let httpResponse = response as? NSHTTPURLResponse {
               
                if httpResponse.statusCode == 200 {
                   
                }
                
            }
        }
 
        dataTask?.resume()*/
        
        let urlString = String(format: "%@%@", baseApiUrl,"/api/v1/product/list")
        let url:NSURL = NSURL(string: urlString)!
        let session = NSURLSession.sharedSession()
        
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "POST"
        request.cachePolicy = NSURLRequestCachePolicy.ReloadIgnoringCacheData
        
        let dictParams: NSDictionary? = ["type":"Yıkama"]
        
        var data = NSData()
        if NSJSONSerialization.isValidJSONObject(dictParams!) { // True
            do {
                data = try NSJSONSerialization.dataWithJSONObject(dictParams!, options: .PrettyPrinted)
            } catch {
                print(">>>> data nil...")
            }
        }
        
        let task = session.uploadTaskWithRequest(request, fromData:data, completionHandler:
            {(data,response,error) in
                
                guard let _:NSData = data, let _:NSURLResponse = response  where error == nil else {
                    print(">>>> error...")
                    //TODO:  call fail method
                    return
                }
                
                let dataString = NSString(data: data!, encoding: NSUTF8StringEncoding)
                print(dataString)
                
                let payload:NSDictionary
                payload = self.convertStringToDictionary(dataString!)!
                print(payload)
                
                print(payload["success"])
                
                var result: NSNumber
                result = (payload["success"] as? NSNumber)!
                
                if(result == 1) {
                    print("request parse result success....")
                    
                    let arr_products:NSMutableArray
                    let mutablePayload = NSMutableDictionary(dictionary: payload)
                    
                    arr_products = mutablePayload.resultObject().mutableCopy() as! NSMutableArray
                    
                    for var i = 0; i < arr_products.count ; ++i {
                        print("-->\(arr_products[i]["name"])") //[[arr_products objectAtIndex:i]valueForKey:@"name"];
                         print("-->\(arr_products[i]["selling_price"])")
                        
                    }
                    
                }
                else {
                    print("request parse result fail....")
                }
                
                //TODO: call fail method
            }
        );
        
        task.resume()
        
    }
    
    
    func initiateLoadingView(baseView:UIView)  {
        
        loadingView = UIView(frame: CGRectMake( ((UIScreen.mainScreen().bounds.size.width - 100) / 2), ((UIScreen.mainScreen().bounds.size.height - 100) / 2), 100, 100))
        loadingView.backgroundColor = UIColor(white: 0.0, alpha: 0.6)
        loadingView.layer.cornerRadius = 5
        
        let activityIndicatorView = UIActivityIndicatorView()
        activityIndicatorView.center = CGPointMake(loadingView.bounds.size.width/2, 35)
        activityIndicatorView.startAnimating()
        activityIndicatorView.viewWithTag(100)
        loadingView.addSubview(activityIndicatorView)
        
        let lblLoading = UILabel(frame: CGRectMake(-5, 58, 110, 30))
        lblLoading.text = "Bekleyiniz..."
        lblLoading.textColor = UIColor.whiteColor()
        lblLoading.font = UIFont(name: "Roboto-Bold", size: 15)
        lblLoading.textAlignment = NSTextAlignment.Center
        loadingView.addSubview(lblLoading)
        
        baseView.addSubview(loadingView)
    }
    
    func removeLoadingView(baseView:UIView)  {
        
        loadingView.hidden = true
        loadingView.removeFromSuperview()
        
    }
    
    func convertStringToDictionary(text: NSString) -> [NSString:AnyObject]? {
        if let data = text.dataUsingEncoding(NSUTF8StringEncoding) {
            do {
                let json = try NSJSONSerialization.JSONObjectWithData(data, options: .MutableContainers) as? [String:AnyObject]
                return json
            } catch {
                print("Something went wrong")
            }
        }
        return nil
    }
    
}