//
//  ViewController.swift
//  SwiftTutorial
//
//  Created by Admin on 02/08/16.
//  Copyright © 2016 aktuna. All rights reserved.
//

import UIKit


class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,customCellDelegate {
    
    @IBOutlet weak var myTableView: UITableView!
    
    let cellIdentifier = "customCell"
    let swiftBlogs = ["Ray Wenderlich", "NSHipster", "iOS Developer Tips", "Jameson Quave", "Natasha The Robot", "Coding Explorer", "That Thing In Swift", "Andrew Bancroft", "iAchieved.it", "Airspeed Velocity"]
    let arrColors = [UIColor.blueColor(), UIColor.yellowColor(), UIColor.redColor(), UIColor.orangeColor(), UIColor.brownColor(),UIColor.grayColor(),UIColor.cyanColor(),UIColor.blackColor(),UIColor.darkGrayColor(),UIColor.purpleColor()]

    //MARK:LifeCycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myTableView.delegate = self
        myTableView.dataSource = self
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    //MARK:TableView Delegate Methods
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        //--return 1
        return swiftBlogs.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //--return swiftBlogs.count
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        //** THIS FOR DEFAULT (STYLE BASIC) TABLE CELL CREATION
        //let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath)
        //cell.textLabel?.text = swiftBlogs[indexPath.row]
        //**
        
        //** THIS FOR CUSTOM TABLE CELL CREATION
        let cell:CustomCell = self.myTableView.dequeueReusableCellWithIdentifier(cellIdentifier) as! CustomCell
        
        cell.cellView.backgroundColor = self.arrColors[indexPath.section]
        cell.cellLabel.text = self.swiftBlogs[indexPath.section]
        cell.btnClick.tag = indexPath.section
        
        cell.myDelegate = self
        //**
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //--print("You tapped cell number \(indexPath.row).")
        let strIndex = String(indexPath.section)
        self.initiateAlertView(strIndex)
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30.0
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionView = UIView()
        
        let lblTitle = UILabel(frame: CGRectMake(10, 5, UIScreen.mainScreen().bounds.size.width, 20))
        lblTitle.textAlignment = NSTextAlignment.Center
        lblTitle.textColor = UIColor.greenColor()
        lblTitle.text = "Section Title"
        sectionView.addSubview(lblTitle)
        //--self.mainView.addSubview(label)
        
        sectionView.backgroundColor = self.arrColors[section]
        return sectionView
        
    }
    
    //MARK:Cell Delegate Methods
    func buttonClickedAction(cellController:CustomCell, btnSender:AnyObject) {
        //--print("You tapped button \(swiftBlogs[cellController.btnClick.tag])")
        self.initiateAlertView(swiftBlogs[cellController.btnClick.tag])
    }
    
    //MARK: Util Methods
    func initiateAlertView(clickedCellName:String) {
        //--let secondViewController = self.storyboard!.instantiateViewControllerWithIdentifier("viewController2") as! ViewController2
        //--self.navigationController!.pushViewController(secondViewController, animated: true)
        
        let alertController = UIAlertController(title: "Basılan", message: clickedCellName, preferredStyle: .Alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) {
            UIAlertAction in
            //--NSLog("OK Pressed")
            
            let secondViewController = self.storyboard!.instantiateViewControllerWithIdentifier("viewController2") as! ViewController2
            self.navigationController!.pushViewController(secondViewController, animated: true)
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel) {
            UIAlertAction in
            NSLog("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.presentViewController(alertController, animated: true, completion: nil)
    }

}

