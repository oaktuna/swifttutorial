//
//  CustomCell.swift
//  SwiftTutorial
//
//  Created by Admin on 02/08/16.
//  Copyright © 2016 aktuna. All rights reserved.
//

import Foundation
import UIKit

protocol customCellDelegate {
    func buttonClickedAction(cellController:CustomCell, btnSender:AnyObject)
}

class CustomCell: UITableViewCell {
    
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var btnClick: UIButton!
    
    
    var myDelegate:customCellDelegate! = nil
    
    
    @IBAction func btnClickAction(sender: AnyObject) {
        
        myDelegate.buttonClickedAction(self, btnSender: sender)
        
    }
    
}
