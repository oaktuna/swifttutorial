//
//  ProductsModel.swift
//  SwiftTutorial
//
//  Created by OMER AKTUNA on 12/08/16.
//  Copyright © 2016 aktuna. All rights reserved.
//

import Foundation
import UIKit

class ProductsModel {
    // MARK: Properties
    
    var name: String = ""
    var image: UIImage?
    var productId: Int = 0
    
    
    init(name: String, image: UIImage, productId: Int) {
        self.name = name
        self.image = image
        self.productId = productId
    }
}