//
//  ProductsData.swift
//  SwiftTutorial
//
//  Created by Admin on 04/08/16.
//  Copyright © 2016 aktuna. All rights reserved.
//

import Foundation

extension NSDictionary {
    
    func resultObject() -> NSArray {
        return self["result"] as! NSArray
    }
    
    func nameAttribute() -> NSString {
        return self["name"] as! NSString
    }
    
    func productIdAttribute() -> NSNumber {
        return self["productId"] as! NSNumber
    }
}
